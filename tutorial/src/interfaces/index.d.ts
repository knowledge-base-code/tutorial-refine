export const published = Symbol('statsu:published')
export const draft = Symbol('status:draft')
export const rejected = Symbol('status:rejected')

enum status {
    published = published,
    draft = draft,
    rejected = rejected
}

export interface IPost {
    id: string
    title: string
    status: status.published | status.draft | status.rejected
    createdAt: string
}
