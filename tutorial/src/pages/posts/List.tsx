import {
    DateField,
  List, Table, TagField, useTable
} from '@pankod/refine'

import { IPost } from 'interfaces'

export const PostsList: React.FC = () => {
  const { tableProps } = useTable<IPost>() // Fetch the data here and make it available for AntD <Table />
  
  return (
    <List >
      <Table {...tableProps} rowKey='id'>
	<Table.Column dataIndex='title' title='title' />
	<Table.Column dataIndex='status' title='status' render={value => <TagField value={value} />} />
	<Table.Column dataIndex='createdAt' title='createdAt' render={value => <DateField value={value} format='LLL'/>} />
      </Table>
    </List>
  )
}
