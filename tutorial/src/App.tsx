import { Refine, Resource } from '@pankod/refine'
import dataProvider from '@pankod/refine-simple-rest'
import '@pankod/refine/dist/styles.min.css'
import { PostsList } from 'pages/posts/List'

export const App: React.FC = () => {
  return (
    <Refine
      dataProvider={dataProvider('https://api.fake-rest.refine.dev')}
    >
      <Resource name="posts" list={PostsList} />
    </Refine>
  )
}
